const mongoose = require('mongoose');

const DepoimentoSchema = new mongoose.Schema({
    dataCadastro: Date,
    dataEdicao: Date,
    texto: String,
    titulo: String,
    autor: String,
    email: String,
    telefone: String,
    excluido: Boolean
});

module.exports = mongoose.model('Depoimento', DepoimentoSchema);