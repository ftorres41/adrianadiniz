const mongoose = require('mongoose');

const UsuarioSchema = new mongoose.Schema({
    nome: String,
    dataNascimento: Date,
    email: String,
    telefones: [String],

});

module.exports = mongoose.model('Usuario', UsuarioSchema);