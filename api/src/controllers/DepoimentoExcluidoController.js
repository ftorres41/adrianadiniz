const Depoimento = require('../models/Depoimento');

module.exports = {
    async index(req, res) {
        const depoimentos = await Depoimento.find({ excluido: true });

        return res.json(depoimentos);
    },

    async show(req, res) {
        const { quantidade } = req.params;

        const depoimentos = await Depoimento.find({ excluido: true })
            .limit(quantidade)
            .sort({ dataCadastro: 1 });

        return res.json(depoimentos);
    },

    async update(req, res) {
        const { id } = req.params;

        await Depoimento.findByIdAndUpdate(id, {
            excluido: false
        }, function (depoimento) {
            return res.json(depoimento);
        });
    },

    async destroy (req, res) {
        const { id } = req.params;

        await Depoimento.findByIdAndDelete(id, function () {
            return res.status(200).json({ message: 'Depoimento excluído permanentemente com sucesso' })
        });
    }
};