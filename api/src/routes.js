const express = require('express');
const multer = require('multer');
const uploadConfig = require('./config/upload');

const MenuController = require('./controllers/MenuController');
const DepoimentoController = require('./controllers/DepoimentoController');
const DepoimentoExcluidoController = require('./controllers/DepoimentoExcluidoController');

const routes = express.Router();
const upload = multer(uploadConfig);

routes.get('/menu', MenuController.index);

routes.get('/depoimentos', DepoimentoController.index);
routes.get('/depoimentos/:quantidade', DepoimentoController.show);
routes.post('/depoimentos', DepoimentoController.store);
routes.post('/depoimentos/editar', DepoimentoController.update);
routes.post('/depoimentos/excluir/:id', DepoimentoController.destroy);

routes.get('/depoimentos/excluidos', DepoimentoExcluidoController.index);
routes.get('/depoimentos/excluidos/:quantidade', DepoimentoExcluidoController.show);
routes.post('/depoimentos/excluidos/editar', DepoimentoExcluidoController.update);
routes.post('/depoimentos/excluidos/remover/:id', DepoimentoExcluidoController.destroy);

module.exports = routes;