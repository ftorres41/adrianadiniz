const Depoimento = require('../models/Depoimento');

module.exports = {
    async index(req, res) {
        const depoimentos = await Depoimento.find({ excluido: false });

        return res.json(depoimentos);
    },

    async show(req, res) {
        const { quantidade } = req.params;

        const depoimentos = await Depoimento.find({ excluido: false })
            .limit(quantidade)
            .sort({ dataCadastro: 1 });

        return res.json(depoimentos);
    },

    async store(req, res) {
        const { texto, titulo, autor, email, telefone } = req.body;

        if (!texto || !autor) {
            return res.status(400).json({ message: 'Depoimento incompleto! Favor verificar valores' })
        }

        const depoimento = await Depoimento.create({
            texto,
            titulo,
            autor,
            email,
            telefone,
            dataCadastro: new Date(),
            dataEdicao: new Date(),
            excluido: false
        });

        return res.json(depoimento);
    },

    async update(req, res) {
        const { id, texto, titulo, autor, email, telefone } = req.body;

        await Depoimento.findByIdAndUpdate(id, {
            titulo: titulo,
            texto: texto,
            autor: autor,
            email: email,
            telefone: telefone
        }, function (depoimento) {
            return res.json(depoimento);
        });
    },

    async destroy(req, res) {
        const { id } = req.params;

        await Depoimento.findByIdAndUpdate(id, {
            excluido: true
        }, function () {
            return res.status(200).json({ message: 'Depoimento excluído com sucesso. Você pode restaurá-lo na Lixeira' })
        });
    },
};