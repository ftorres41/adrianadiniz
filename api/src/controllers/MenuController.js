const Menu = require('../models/Menu');

module.exports = {
    async show(req, res) {
        const { user_id } = req.headers;

        if (!user) {
            return res.status(400).json({ message: 'Usuário não existe!' })
        }

        const menu = await Menu.find({ user: user_id });

        return res.json(menu);
    }
}